#----------------------NCMPCPP----------------------
mkdir -p $HOME/.ncmpcpp &
ln -sf $PWD/ncmpcpp-config $HOME/.ncmpcpp/config &
#---------------------------------------------------


#----------------------MPD----------------------
mkdir -p $HOME/.config/mpd &
ln -sf $PWD/mpd-config $HOME/.config/mpd/mpd.conf &
#-----------------------------------------------


#----------------------I3----------------------
mkdir -p $HOME/.config/i3 &
ln -sf $PWD/i3-config $HOME/.config/i3 &
#----------------------------------------------


#----------------------I3-STATUS----------------------
mkdir -p $HOME/.config/i3 &
ln -sf $PWD/i3status-config $HOME/.i3status.conf &
#-----------------------------------------------------


#----------------------SPACEMACS----------------------
ln -sf $PWD/spacemacs-config $HOME/.spacemacs &
#-----------------------------------------------------

